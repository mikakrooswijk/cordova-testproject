import { Component, OnInit } from '@angular/core';
declare let device;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  deviceplatform: string;

  ngOnInit() {
    document.addEventListener("deviceready", function() { 
      }, false);
  }

  onClick() {
    this.deviceplatform = window['plugins'].toastyPlugin.show();
  }
}
