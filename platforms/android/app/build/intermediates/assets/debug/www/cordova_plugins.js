cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-toastyplugin.toastyPlugin",
    "file": "plugins/cordova-plugin-toastyplugin/www/plugin.js",
    "pluginId": "cordova-plugin-toastyplugin",
    "clobbers": [
      "window.plugins.toastyPlugin"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-whitelist": "1.3.3",
  "cordova-plugin-device": "2.0.2",
  "cordova-plugin-toastyplugin": "0.0.1"
};
// BOTTOM OF METADATA
});