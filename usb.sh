#!/usr/bin/env bash

# Set Variables
idVendor=$1
idProduct=$2

# Give access to USB device
sudo usermod -a -G plugdev vagrant
sudo touch /etc/udev/rules.d/51-android.rules
echo "SUBSYSTEM==\"usb\", ATTR{idVendor}==\"$idVendor\", ATTR{idProduct}==\"$idProduct\", MODE=\"0660\", GROUP=\"plugdev\", SYMLINK+=\"android%n\"" | sudo tee -a /etc/udev/rules.d/51-android.rules
