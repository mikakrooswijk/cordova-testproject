#!/usr/bin/env bash
ANDROID_SDK_FILENAME=android-sdk_r26.1.1-linux.tgz
ANDROID_SDK=http://dl.google.com/android/$ANDROID_SDK_FILENAME

sudo apt-get update

sudo apt-get install -y expect
sudo apt-get install -y unzip

# install nodejs & npm
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# install cordova and angular
sudo npm install -g cordova
sudo npm install -g @angular/cli

# install java
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo apt-get -y install oracle-java8-installer

#install android SDK
curl -O $ANDROID_SDK
tar -xzvf $ANDROID_SDK_FILENAME
sudo chown -R vagrant android-sdk-linux/
sudo mv android-sdk-linux/ /home/vagrant/
rm $ANDROID_SDK_FILENAME

echo "ANDROID_HOME=~/android-sdk-linux" >> /home/vagrant/.bashrc
echo "PATH=\$PATH:~/android-sdk-linux/tools:~/android-sdk-linux/platform-tools" >> /home/vagrant/.bashrc

# expect -c '
# set timeout -1   ;
# spawn /home/vagrant/android-sdk-linux/tools/android update sdk -u --all --filter platform-tool,android-23,build-tools-23.0.3
# expect { 
#     "Do you accept the license" { exp_send "y\r" ; exp_continue }
#     eof
# }
# '

# install ant
sudo apt-get install -y ant

# install gradle
sudo wget https://services.gradle.org/distributions/gradle-2.4-bin.zip
sudo unzip gradle-2.4-bin.zip -d /opt/
sudo rm -rf gradle-2.4-bin.zip 

echo "export PATH=\$PATH:/opt/gradle-2.4/bin" >> /home/vagrant/.bashrc

# permission to write to folder
sudo chmod -R 777 /home/vagrant/android-sdk-linux

# update android SDK
echo y | android update sdk --no-ui --filter tools,platform-tools,build-tools-28.0.2,android-22

# revert permission
sudo chmod -R 775 /home/vagrant/android-sdk-linux
