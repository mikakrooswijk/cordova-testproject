#!/usr/bin/env bash

ANDROID_SDK_FILENAME=sdk-tools-linux-4333796.zip
ANDROID_SDK=http://dl.google.com/android/repository/$ANDROID_SDK_FILENAME
GRADLE_VERSION=gradle-4.10
GRADLE_FILENAME=$GRADLE_VERSION-bin.zip
GRADLE=https://services.gradle.org/distributions/$GRADLE_FILENAME
ANDROID_ROOT=/home/vagrant/android-sdk

# Set Environment Variables
echo "export JAVA_HOME=\"/usr/lib/jvm/java-8-oracle\"" >> /home/vagrant/.bashrc
echo "export ANDROID_HOME=\"/home/vagrant/android-sdk\"" >> /home/vagrant/.bashrc
echo "export PATH=\"$PATH:$ANDROID_ROOT/tools/bin:$ANDROID_ROOT/platform-tools:$ANDROID_ROOT/tools:$ANDROID_ROOT/build-tools:/opt/gradle/$GRADLE_VERSION/bin\"" >> /home/vagrant/.bashrc
source /home/vagrant/.bashrc

# Install unzip utility
sudo apt-get install -y unzip

# Install Nodejs & NPM
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential

# Install Cordova
sudo npm install --global cordova
cordova telemetry off

# Install Angular
sudo npm install --global @angular/cli

# Install Java
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
sudo apt-get install -y oracle-java8-installer

# Install Android SDK
sudo wget $ANDROID_SDK
sudo unzip $ANDROID_SDK_FILENAME -d /android-sdk/
sudo chown -R vagrant /android-sdk/
sudo mv /android-sdk/ /home/vagrant/
sudo rm $ANDROID_SDK_FILENAME

# Update Android SDK
sudo touch /root/.android/repositories.cfg
sudo $ANDROID_ROOT/tools/bin/sdkmanager --update
yes | sudo $ANDROID_ROOT/tools/bin/sdkmanager "platforms;android-26" "build-tools;26.0.2" "platform-tools" "tools"
yes | sudo $ANDROID_ROOT/tools/bin/sdkmanager --licenses

# Install Gradle
sudo wget $GRADLE
sudo unzip $GRADLE_FILENAME -d /opt/gradle/
sudo rm $GRADLE_FILENAME
